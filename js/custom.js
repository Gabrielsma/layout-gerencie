/* ------------------ Mostrar filtros avançados ----------------------*/
$("#toggle-search").click(function () {
    $("#advanced-search").toggle();
});

/* ---------------------- Datas no dropdown --------------------------*/
$('#dt-fim').hide();
$('#label-ate').hide();

$('#hoje').click(function () {
    $('#dt-fim').hide();
    $('#label-ate').hide();
    $('#dt-fim').val('');
    $('#date-dynamic').val('');
    $('#date-dynamic').attr('type','date');
    $('#date-dynamic').val(moment().format('YYYY-MM-DD'));
});

$('#semana').click(function () {
    $('#dt-fim').hide();
    $('#label-ate').hide();
    $('#dt-fim').val('');
    $('#date-dynamic').val('');
    $('#date-dynamic').attr('type','week');
    $('#date-dynamic').val(moment().format('YYYY') + '-W' + moment().format('ww'));
});

$('#mes').click(function () {
    $('#dt-fim').hide();
    $('#label-ate').hide();
    $('#dt-fim').val('');
    $('#date-dynamic').val('');
    $('#date-dynamic').attr('type','month');
    $('#date-dynamic').val(moment().format('YYYY-MM'));
});

$('#periodo').click(function () {
    $('#dt-fim').val('');
    $('#date-dynamic').val('');
    $('#date-dynamic').attr('type','date');
    $('#dt-fim').show();
    $('#label-ate').show();
});

/* --------------- Chamar cadastros de usuários modal - Tela de compromisso -----------------*/

$('#select_usuario').change(function (){
    var selected = $('#select_usuario option:selected').val();
    if(selected == '-1') {
        $('#CadastroUsuarioModal').modal('show');
    }
});

/* --------------- Chamar cadastros de Area de Atuacao - Tela de solitacoes -----------------*/

$('#area-new').change(function (){
    var selected = $('#area-new option:selected').val();
    if(selected == '-1') {
        $('#CadastroAreaModal').modal('show');
    }
});

/* --------------- Copiar link de convite - Tela de usuarios -----------------*/
$("#convite-link").hide();
$("#convite").click(function () {
    $("#convite-link").show();
    $("#convite-link").focus();
    $("#convite-link").select();
    document.execCommand("copy")
    $("#convite-link").hide();
    alert('Link do convite copiado!');
});